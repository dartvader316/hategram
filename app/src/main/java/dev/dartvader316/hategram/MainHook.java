package dev.dartvader316.hategram;

import java.util.Arrays;
import java.util.List;
import java.lang.reflect.Field;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class MainHook implements IXposedHookLoadPackage {
    public final static List<String> hookPackages = Arrays.asList(
            "org.telegram.messenger",
            "org.telegram.messenger.web",
            "org.telegram.messenger.beta",
            "nekox.messenger",
            "com.cool2645.nekolite",
            "org.telegram.plus",
            "com.iMe.android",
            "org.telegram.BifToGram",
            "ua.itaysonlab.messenger",
            "org.forkclient.messenger",
            "org.forkclient.messenger.beta",
            "org.aka.messenger",
            "ellipi.messenger",
            "org.nift4.catox",
            "it.owlgram.android");

    @Override
    public void handleLoadPackage(final XC_LoadPackage.LoadPackageParam lpparam) {
        if (hookPackages.contains(lpparam.packageName)) {
            try {
                Class<?> messagesControllerClass = XposedHelpers.findClassIfExists("org.telegram.messenger.MessagesController", lpparam.classLoader);
                if (messagesControllerClass != null) {
                    XposedBridge.hookAllMethods(messagesControllerClass, "getSponsoredMessages", XC_MethodReplacement.returnConstant(null));
                    XposedBridge.hookAllMethods(messagesControllerClass, "isChatNoForwards", XC_MethodReplacement.returnConstant(false));

                    // Set all users non premium by default
                    XposedBridge.hookAllMethods(messagesControllerClass, "isPremiumUser", XC_MethodReplacement.returnConstant(false));
                }
                Class<?> chatUIActivityClass = XposedHelpers.findClassIfExists("org.telegram.ui.ChatActivity", lpparam.classLoader);
                if (chatUIActivityClass != null) {
                    XposedBridge.hookAllMethods(chatUIActivityClass, "addSponsoredMessages", XC_MethodReplacement.returnConstant(null));
                }
                Class<?> SharedConfigClass = XposedHelpers.findClassIfExists("org.telegram.messenger.SharedConfig", lpparam.classLoader);
                if (SharedConfigClass != null) {
                    XposedBridge.hookAllMethods(SharedConfigClass, "getDevicePerformanceClass", XC_MethodReplacement.returnConstant(2));
                }
                Class<?> UserConfigClass = XposedHelpers.findClassIfExists("org.telegram.messenger.UserConfig", lpparam.classLoader);
                if (UserConfigClass != null) {
                    XposedBridge.hookAllMethods(UserConfigClass, "getMaxAccountCount", XC_MethodReplacement.returnConstant(999));

                    // I dont care about premium features
                    XposedBridge.hookAllMethods(UserConfigClass, "hasPremiumOnAccounts", XC_MethodReplacement.returnConstant(false));
                    XposedBridge.hookAllMethods(UserConfigClass, "isPremium", XC_MethodReplacement.returnConstant(false));
                }
                Class<?> DialogsActivityClass = XposedHelpers.findClassIfExists("org.telegram.ui.DialogsActivity", lpparam.classLoader);
                if (DialogsActivityClass != null){
                    // Hide all premium hints (like premium sales)
                    XposedBridge.hookAllMethods(DialogsActivityClass, "isPremiumRestoreHintVisible", XC_MethodReplacement.returnConstant(false));
                    XposedBridge.hookAllMethods(DialogsActivityClass, "isPremiumChristmasHintVisible", XC_MethodReplacement.returnConstant(false));
                    XposedBridge.hookAllMethods(DialogsActivityClass, "isPremiumHintVisible", XC_MethodReplacement.returnConstant(false));
                }
                
                Class<?> userObjectClass = XposedHelpers.findClassIfExists("org.telegram.messenger.userObject", lpparam.classLoader);
                if (userObjectClass != null){
                    // Remove emojis from profile
                    XposedBridge.hookAllMethods(userObjectClass, "getEmojiStatusDocumentId", XC_MethodReplacement.returnConstant(null));
                    XposedBridge.hookAllMethods(userObjectClass, "getEmojiId", XC_MethodReplacement.returnConstant(0));
                    // Remove custom premium colors
                    XposedBridge.hookAllMethods(userObjectClass, "getColorId", XC_MethodReplacement.returnConstant(0));
                }

                Class<?> chatMessageCellClass = XposedHelpers.findClassIfExists("org.telegram.ui.Cells.ChatMessageCell", lpparam.classLoader);
                if (chatMessageCellClass != null){
                    // Remove premium icon in message cell
                    XposedBridge.hookAllMethods(chatMessageCellClass, "getAuthorStatus", XC_MethodReplacement.returnConstant(null));
                }

                Class<?> chatActivityEnterVIew = XposedHelpers.findClassIfExists("org.telegram.ui.Components.ChatActivityEnterView", lpparam.classLoader);
                if(chatActivityEnterVIew != null){
                    // remove gift premium button in private messages
                    XposedBridge.hookAllMethods(chatActivityEnterVIew, "updateGiftButton", XC_MethodReplacement.DO_NOTHING);
                    XposedBridge.hookAllMethods(chatActivityEnterVIew, "createGiftButton", XC_MethodReplacement.DO_NOTHING);
                }

                Class<?> ReplyMessageLine = XposedHelpers.findClassIfExists("org.telegram.ui.Components.ReplyMessageLine", lpparam.classLoader);
                if (ReplyMessageLine != null){
                    // Remove emojis in reply quote background
                    XposedBridge.hookAllMethods(ReplyMessageLine, "isEmojiLoaded", XC_MethodReplacement.returnConstant(false));
                    // Remove multicolor lines for premiums in reply quote 
                    XposedBridge.hookAllMethods(ReplyMessageLine, "resolveColor", new XC_MethodHook() {
                        @Override
                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {}

                        @Override
                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                            /*
                            thisObject.hasColor2 = false;
                            thisObject.hasColor3 = false;
                            */
                            Object thisObject = param.thisObject;

                            Field[] fields = thisObject.getClass().getDeclaredFields();

                            for(Field f:fields){
                                if(f.getName().equals("hasColor2") || f.getName().equals("hasColor3")){
                                    if(!f.isAccessible()){
                                        f.setAccessible(true);
                                        Class<?> type = f.getType();
                                        f.set(thisObject, false);
                                        
                                        f.setAccessible(false);
                                    }
                                }
                            }
                            
                        }
                    });
                }


            } catch (Throwable ignored) {
            }
        }
    }
}
